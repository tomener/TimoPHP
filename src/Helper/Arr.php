<?php
/**
 * Arr.php
 * User: tommy
 */


namespace Timo\Helper;


class Arr
{
    /**
     * 深度合并
     *
     * @param $array
     * @param $tobe_merge_array
     */
    public static function deepmerge(&$array, $tobe_merge_array)
    {
        foreach ($tobe_merge_array as $key => $item) {
            if (is_array($item)) {
                if (isset($array[$key]) && is_array($array[$key])) {
                    //递归深度合并
                    self::deepmerge($array[$key], $item);
                } else {
                    $array[$key] = $item;
                }
            } else {
                $array[$key] = $item;
            }
        }
    }
}
